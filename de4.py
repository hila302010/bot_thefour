
def do_turn(pw):
	
	if len(pw.my_fleets()) >= 1:
		return
	num_ships = 0
	dest = 0
	ret_lanet = 0
	#distance = pw.distance(pw.planets()[1],pw.my_planets()[0]) 
	if len(pw.my_planets()) == 0:
		return
	source = pw.my_planets()[0]
	dest = pw.planets()[0]
	distance = pw.distance(source, dest)
	if len(pw.neutral_planets()) >= 1:
		for planet in pw.planets():
			if (planet.planet_id() != pw.my_planets()[0].planet_id()) and planet.owner() == 0:
				if (pw.distance(planet, pw.my_planets()[0]) < distance):
					distance = pw.distance(planet, pw.my_planets()[0])
					dest = planet
					num_ships = source.num_ships() / 2
		
	if len(pw.my_planets()) > 3:
		for planet in pw.my_planets():
			for enemy in pw.enemy_planets():
				if planet.num_ships() > enemy.num_ships():
					source = planet
					dest = enemy
					num_ships = source.num_ships() / 2;
					pw.issue_order(source, dest, num_ships)
	
	
	
	else:
		pw.issue_order(source, dest, num_ships)
